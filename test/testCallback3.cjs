const lists = require('../data/lists_1.json')
const boards = require('../data/boards_1.json')
const cards = require('../data/cards_1.json')

const cardDetailsWithListId = require('../callback3.cjs')

const listId = 'qwsa221'
cardDetailsWithListId(listId, cards)
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
