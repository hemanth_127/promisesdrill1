// // - callback with nested

// function step1 (data, callback) {
//     if (!data) {
//       console.log('err at 1')
//       throw new Error('err')
//     }
//     console.log('step is succseeful at;', data)
//     callback(null, data+1)
//   }
//   function step2 (data, callback) {
//     if (!data) {
//       console.log('err at 2')
//       throw new Error('err')
//     }
//     console.log('step is succseeful at;', data)
//     callback(null, data+1)
//   }
//   function step3 (data, callback) {
//     if (!data) {
//       console.log('err at 3')
//       throw new Error('err')
//     }
//     console.log('step is succseeful at;', data)
//     callback(null, data+1)
//   }
//   function step4 (data, callback) {
//     if (!data) {
//       console.log('err at 4')
//       throw new Error('err')
//     }
//     console.log('step is succseeful at;', data)
//     callback(null, data+1)
//   }

//   step1(1, (err,data)=> {
//       console.log("hell paradise");
//       step2(data,(err,data)=>{
//           step3(data,(err,data)=>{
//               step4(data,(err,data)=>{
//                   console.log("completed");
//               })
//           })
//       })
//   })

// ------------------------------------------------

cart = ['shoes', 'phants', 'hellshit']

function validateCart () {
  return true
}

function createOrder (cart) {
  return new Promise(function (resolve, reject) {
    if (!validateCart(cart)) {
      const err = new Error('cart is not valid')
      reject(err)
    }
    const orderId = '12435'
    if (orderId) {
      resolve(orderId)
    }
  })
}

function proceeedToPayment (orderId) {
  return new Promise((resolve, reject) => {
    // setTimeout(function () {
    resolve(orderId)
    // }, 5000)
  })
}

function paymentInfo (orderId) {
  return new Promise((resolve, reject) => {
    // if (orderId === "12435") {
    resolve('shoes bought successfully')
    // }
  })
}

function updateWallet (orderId) {
  return new Promise((resolve, reject) => {
    if (orderId === '12435') {
      orderId = '98468563487568732'
      resolve('updated successfull')
    }
  })
}

// --one way of calling promises

// const promise = new createOrder(cart)

// promise.then(function (orderId) {
//   console.log(orderId)
//   return orderId
// })
// ___________________________________

// Another way calling Function

// createOrder(cart)
//   .then(function (orderId) {
//     let k = proceeedToPayment(orderId)
//     console.log(1, k)
//     return orderId
//   })
// .then(function (orderId) {
//   const p = paymentInfo(orderId)
//   // console.log(2, p)
//   return
// })
// .then(orderId => {
//   const l = updateWallet((orderId = '12435'))
//   // console.log(3, l)
//   return orderId
// })

// async function helper( orderId){
//   console.log(orderId)
//   const z= await proceeedToPayment(orderId)
//   console.log("help",z)
// }
// helper()

// const p1 = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve('p1 sucess')
//     reject('p1 fail')
//   }, 5000)
// })

// const p2 = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve('p2 sucess')
//     reject('p2 fail')
//   }, 2000)
// })

// const p3 = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve('p3 sucess')
//     reject('p3 fail')
//   }, 7000)
// })

// promises with any,all,allsettled,race,then,catch

// Promise.all([p1, p2, p3])
//   .then(p => console.log(p))
//   .catch(err => console.log('its rejected'))

// Promise.allSettled([p1, p2, p3])
//   .then(p => console.log(p))
//   .catch(err => console.log('its error'))

// Promise.race([p1, p2, p3])
//   .then(p => console.log(p))
//   .catch(err => console.log('its error'))

// Promise.any([p1, p2, p3])
//   .then(p => console.log(p))
//   .catch(err => console.log(err.errors))

/* Write a basic implementation of Promise.all that accepts an array 
of promises and return another array with the data coming from all the promises.
 Make sure if any of the Promise gets rejected throw error. Only when all the promises
  are fulfilled resolve the promise. */

function promiseAll (promise) {

  return new Promise((resolve, reject) => {
    Promise.all(promise).then(res => {
      resolve(res)
    })
    .catch((err)=>{
     reject(err)
    })
  })
}

// Test:
let times = [1, 2, 3, 4]
let timesPromise = times.map(second =>
   new Promise(res => { setTimeout(() => res(Math.random()), second * 1000)
    })
)

promiseAll(timesPromise).then(console.log)
